<?php

add_action( 'wp_footer', function() {

	/**
	 * Check if this is an RTL language
	 */
	if ( ! is_rtl() ) {
		return;
	}

	$starter = Starter::get_instance();
	$less_files = glob( $starter->plugin_dir_path . 'inc/less-rtl/' . '*.less' );
	$less_code = '';
	foreach ( $less_files as $less_file ) {
		$less_code .= file_get_contents( $less_file );
	}
	$less = new lessc;
	printf( '<style>%s</style>', $less->compile( $less_code ) );
}, 999998 );
